from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse
from .models import Post


class BlogTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(username="dummy_user", email="test@mail.com", password="secret")
        self.post = Post.objects.create(title="Test Post", author=self.user, body="Test Body")

    def test_post_list_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Test Body")
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('post/99999/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, "Test Post")
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('post_new'), {
            'title': 'test dummy title',
            'body': 'test dummy body',
            'author': self.user,
        })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "test dummy title")
        self.assertContains(response, "test dummy body")

    def test_post_update_view(self):
        response = self.client.post(reverse('post_edit', args='1'),
                                    {'title': 'updated title', 'body': 'updated body text'})

        self.assertEqual(response.status_code, 302)

    def test_post_delete_view(self):
        response = self.client.get(reverse('post_delete', args='1'))
        self.assertEqual(response.status_code, 200)
